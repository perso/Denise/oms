#!/usr/bin/env python3
# -*- coding: utf-8 -*-


### Gérer les couleurs

from gestion_erreurs import warning

def rgb_vers_tuple(chaine,defaut,liste_err):
    """ convertit une chaine rgb genre #00FF1B
    vers un tuple python (entre 0 et 1)
    En cas d'erreur, on met un warning et on renvoie
    le defaut"""
    chaine2 = chaine.lstrip("#")
    try:
        r = int(chaine2[0:2],16)/255
        v = int(chaine2[2:4],16)/255
        b = int(chaine2[4:6],16)/255                  
    except:
        warning("Impossible de convertir la couleur "+chaine,liste_err)
        return defaut
    return (r,v,b)

def tuple_vers_rgb( t):
    """ conversion inverse : renvoie une chaine
    de type #FF0045"""
    (r,v,b) = t
    if (r>1):
        r=1
    elif r<0:
        r=0
    if (v>1):
        v=1
    elif v<0:
        v=0
    if (b>1):
        b=1
    elif b<0:
        b=0        
        
    r = int(r*255)
    v = int(v*255)
    b = int(b*255)
    return '#%02x%02x%02x' % (r,v,b)

def progressif(x,y,nb):
    """ renvoie x pour 0, y pour 50, 
    et entre deux pour les intermédiaires"""
    return (x*(50-nb)/50 + y*nb/50)

def degrade_choix(col1,col2,col3,nb):
    """ retourne un dégradé entre col1 et col2 (nb<50)
    et entre col2 et col3 (nb>50)"""
    if nb>100:
        nb=100
    elif nb<0:
        nb = 0
        
    if nb<50:
        return tuple( progressif(col1[i],col2[i],nb) for i in (0,1,2) )
    else:
        return tuple( progressif(col2[i],col3[i],nb-50) for i in (0,1,2) ) 