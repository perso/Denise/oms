#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Gestion des erreurs en flask
import sys

niveau_debug = ("debug" in sys.argv)

def initialise_erreurs():
    """ retourne trois listes vides, erreurs fatales (0), warnings(1), debug(2):"""
    return ([],[],[])


def erreur(message,listeerreurs):
    """ en cas d'erreur où on ne peut pas continuer
    message est une chaîne"""
    print("** Erreur fatale : "+message)
    listeerreurs[0].append("** Erreur : "+message)
  
    
def warning(message,listeerreurs):
    """ En cas d'avertissement mais on peut quand même continuer """
    print("** Warning : "+message)
    message = "Alerte : "+message
    if message not in listeerreurs[1]:
        listeerreurs[1].append(message)
    
def debug(message,listeerreurs):
    global niveau_debug
    if niveau_debug:
        print("##Debug : "+message)
        listeerreurs[2].append("# Debug : "+message)
              
