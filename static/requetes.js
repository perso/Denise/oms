
function appelle_image()
{
	affiche_statut("Calcul en cours...") ;
	
	var formData = new FormData( document.getElementById("donnees_enfant") );
	
	var requete = new XMLHttpRequest(); 
	requete.responseType = "json";
	requete.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200) {
			// On nettoie
			nettoie_erreurs() ;
			nettoie_calculs() ;
						
			// on récupère les différents champs de la réponse
			var result = this.response.result ;
			var image_poids = this.response.image_poids ;
			var image_taille = this.response.image_taille ;
			var liste_warnings = this.response.messages
			var texte = this.response.export_txt;
			var nomenfant = this.response.nomenfant ;
			var nomsenfantsplus = this.response.nomsenfantsplus ;
			var calculextra = this.response.calculextra ;

			// on affiche l'export des données
			document.getElementById('export_texte').innerHTML = texte;
			document.getElementById('sectionexport').style.display = "block";
		
			
			var boutondl = document.getElementById("export_dl") ;
			boutondl.setAttribute('onclick',"download_file('donnees_"+nomenfant+".txt', 'application/json;charset=utf-8','"+encodeURIComponent(texte) +"')")
		

			if(result == "success")
			{	
				// On affiche l'image, si elle est là
				if (image_poids != "") {
					document.getElementById('sectioncourbe').style.display = "block";
					document.getElementById('section_courbe_poids').style.display = "block" ;
					document.getElementById('courbe_poids').src = 'data:image/png;base64,'+(image_poids);
					document.getElementById('courbe_poids').alt = 'Courbe de poids de '+nomenfant ;					
					boutondl = document.getElementById("courbe_dl_poids") ;
					boutondl.setAttribute('onclick',"download_file('courbe_poids_"+nomenfant+nomsenfantsplus+".png', 'image/png;base64','"+image_poids +"')")
				}
				else {
					document.getElementById('section_courbe_poids').style.display = "none" ;
				}
				
				if (image_taille != "") {
					document.getElementById('sectioncourbe').style.display = "block";
					document.getElementById('section_courbe_taille').style.display = "block" ;
					document.getElementById('courbe_taille').src = 'data:image/png;base64,'+(image_taille);
					document.getElementById('courbe_taille').alt = 'Courbe de taille de '+nomenfant ;		
					boutondl = document.getElementById("courbe_dl_taille") ;
					boutondl.setAttribute('onclick',"download_file('courbe_taille_"+nomenfant+".png', 'image/png;base64','"+image_taille +"')")
			    }
			    else {
					document.getElementById('section_courbe_taille').style.display = "none" ;						
				}


				// Si y'a eu des warnings, faut les afficher
				if(liste_warnings.length != 0)
				{
					// afficher la liste des warnings
					var elem_div = document.getElementById('courbe_warnings') ;
					elem_div.style.display = "block" ;
					var ul = elem_div.children[1] ;
					for(i=0; i<liste_warnings.length; i++) {
						var li = document.createElement("li");
						li.appendChild(document.createTextNode(liste_warnings[i]));
						ul.appendChild(li);
					}
				}
				// S'il y a des calculs faits, à afficher !
				if(calculextra.length != 0)
				{
					var elem_extr = document.getElementById("section_extradonnees")
					elem_extr.style.display = "block" ;
					var ul2=elem_extr.children[1] ;
					for(i=0; i<calculextra.length; i++) {
						var li = document.createElement("li");
						li.appendChild(document.createTextNode(calculextra[i]));
						ul2.appendChild(li);
					}
				}
				affiche_statut("Courbes générées, scroller vers le bas pour les voir.") ;
			}
			else{ // si la génération de l'image a merdé
				
				// afficher la liste des erreurs
				var elem_div = document.getElementById('courbe_erreurs') ;
				elem_div.style.display = "block" ;
				var ul = elem_div.children[1] ;
				for(i=0; i<liste_warnings.length; i++) {
					var li = document.createElement("li");
					li.appendChild(document.createTextNode(liste_warnings[i]));
					ul.appendChild(li);
				}
				affiche_statut("Il y a eu une erreur à la génération des courbes, voir plus bas.") ;
				
			}
		}
	}
	requete.open("POST","courbe/b64",true)
	requete.send(formData)

}

function nettoie_erreurs()
{
	// fonction qui nettoie les erreurs affichées sur la page
	// vider les warnings
	var elem_div = document.getElementById('courbe_warnings') ;
	elem_div.style.display = "none" ;
	var ul = elem_div.children[1] ;
	ul.innerHTML = "";
	
	// vider les erreurs
	elem_div = document.getElementById('courbe_erreurs') ;
	elem_div.style.display = "none" ;
	ul = elem_div.children[1] ;
	ul.innerHTML = "" ;
	
}

function nettoie_calculs()
{
	// Vider les calculs éventuels
	var elem_extr = document.getElementById("section_extradonnees")
	elem_extr.style.display = "none" ;
	var ul = document.getElementById("extradata") ;
	ul.innerHTML = "" ;	
}


function download_file(filename,mimetype,data) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:'+mimetype+',' + data);
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);	
	
}

function upload_file(nomformulaire) {
	document.getElementById(nomformulaire).submit();
}

// Fonction pour afficher le statut des courbes
function affiche_statut(message) {
	document.getElementById("statut_courbes").innerHTML = message ;
}
