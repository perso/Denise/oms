// fonctions générales pour la page

var taillemaxdate = 10;
var placeholder_date = "aaaa-mm-jj" ;


function ajoutelignes()
{
	// va ajouter 3 lignes au tableau de données
	var table = document.getElementById("donneespoids")
	var nbligne = table.children[0].childElementCount ;
	// les données étant numérotées à partir de 0 on pourra commencer à partir de nbligne -1
	// car il faut éliminer la ligne de header.
	
	// voir dans quel mode on est : date ou texte
	var deuxiemeligne = table.children[0].children[1] ;
	var celldate = deuxiemeligne.children[1] ;
	var mode = celldate.firstChild.type ;
		
	var nbajout = 3 ;
	for(var i=nbligne-1; i<nbligne+nbajout-1; i++)
	{
		var ligne = table.insertRow(i+1);
		var cellage = ligne.insertCell(0);
		var celldate = ligne.insertCell(1);
		var cellpoids = ligne.insertCell(2);
		var celltaille = ligne.insertCell(3);
		
		cellage.innerHTML = '<input type="text" class="data" name="age_'+i+'">' ;
		celldate.innerHTML = '<input name="date_'+i+'">' ;
		celldate.firstChild.type = mode ;
		if(mode == "text") // si on a déjà affiché les cases en mode texte, on continue en mode texte
		{
			celldate.firstChild.size = taillemaxdate ;
			celldate.firstChild.maxlength = taillemaxdate ;
			celldate.firstChild.placeholder = placeholder_date ;
		}
		cellpoids.innerHTML = '<input type="text" class="data" name="poids_'+i+'">' ;	
		celltaille.innerHTML = '<input type="text" class="data" name="taille_'+i+'">' ;
		celltaille.classList.add("donneesplus") ;
		if(deuxiemeligne.children[3].style.display == "block")
			celltaille.style.display = "block" ;
	}
	
}

// Affichage de la section "export"
function affiche_export()
{
	document.getElementById("export").style.display = "block" ;
}
// Copier vers le presse-papiers
function copietexte()
{
  var elt = document.getElementById("export_texte");

  /* Select the text field */
  elt.select();
  elt.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

}

function affiche_cache(id,elemcourant)
{
	// affiche et/ou cache l'élément id, tout en changeant le this
	// en afficher/masquer
	elem = document.getElementById(id)
	if(elem.style.display == "block")
	{
		elem.style.display = "none";
		elemcourant.innerHTML = "Afficher" ;
	}
	else
	{
		elem.style.display = "block" ;
		elemcourant.innerHTML = "Masquer" ;
		
	}
	
}

function affiche_cache_classe(classe)
{
	// affiche et/ou cache tous les éléments de la classe classe (et change le texte du this)
	var listelem = document.getElementsByClassName(classe) ;
	var attributaff ;
	if(listelem[0].style.display != "block") // c'est caché, donc on veut afficher
	{
		document.getElementById("bouton_afficher_donneesplus").innerHTML = "Masquer" ;
		attributaff = "block" ;
	}
	else
	{
		document.getElementById("bouton_afficher_donneesplus").innerHTML = "Afficher" ;
		attributaff = "none" ;
	}
	
	for(var i=0;i<listelem.length; i++)
	{
		listelem[i].style.display = attributaff ;
	}
}

function affiche_donneesplus_sibesoin()
{
	// fonction à appeler au chargement de la page
	// On parcourt le tableau des tailles (ou autres) et on voit si y'a
	// des choses dans les champs
	var trouve = false ;
	var liste_cases = document.getElementsByTagName("td") ;
	//alert(liste_cases[3].children[0].value+liste_cases[3].classList) ;
	for(i=0; i<liste_cases.length && !(trouve); i++)
	{
		// Si une des cases "données plus" contient un truc non vide
		if(liste_cases[i].classList == "donneesplus" && liste_cases[i].children[0].value != "")
		{
			trouve = true ;
		}
	}
	if(trouve)
		affiche_cache_classe("donneesplus")
}

function change_mode_dates(mode)
{
	// passe de l'affichage en mode "date" à l'affichage en mode "texte" pour les navigateurs
	// qui gèrent mal le mode date
	var listeinput = document.getElementsByTagName("input") ;
	for(i=0;i<listeinput.length;i++)
	{
		var nombalise = listeinput[i].name ;
		if(nombalise == "naissance" || nombalise.slice(0,5) == "date_")
		{
			if(mode == "date") {
				listeinput[i].maxlength = taillemaxdate ;
				listeinput[i].value = listeinput[i].value.replace(/\//g, "-") ; // remplacer les / par des -
			}

			listeinput[i].type = mode ;
			listeinput[i].size = taillemaxdate ;
			listeinput[i].placeholder = placeholder_date ;
		}
	}
	
	// changer le texte et le bouton.
	var elem = document.getElementById("changemode") ;
	if(mode =="text") 
	{
		elem.innerHTML = "Saisie des dates au format aaaa-mm-jj. <span onclick=\"change_mode_dates('date')\" class=\"bouton\">Cliquez ici</span> pour revenir à une saisie de dates «&nbsp;confortable&nbsp;» avec le calendrier."
	}
	else
	{
		elem.innerHTML = "<span onclick=\"change_mode_dates('text')\" class=\"bouton\">Cliquez ici</span> pour saisir les dates comme du texte."
	}
}

function ajoute_enfants()
{
	var eltul = document.getElementById("liste_enfants_add") ;
	var nblignes = eltul.childElementCount ;
	var elt1 = eltul.children[0] ;
	//alert(elt1.innerHTML) ;
	var texte = elt1.innerHTML ; 
	// Il faut remplacer les nblignes 2 par des nblignes +2
	texte = texte.replace("fichier_donnees_"+(2), "fichier_donnees_"+(nblignes +2)) ;
	texte = texte.replace("fichier_donnees_"+(2), "fichier_donnees_"+(nblignes +2)) ;
	texte = texte.replace("fichier_donnees_"+(2), "fichier_donnees_"+(nblignes +2)) ;
	texte = texte.replace("symbole_donnees_"+(2), "symbole_donnees_"+(nblignes +2)) ;
	texte = texte.replace("couleur_donnees_"+(2), "couleur_donnees_"+(nblignes +2)) ;

	// Enlever le "selected"
	texte = texte.replace("selected", "") ;

	var elt2 = document.createElement("li") ;
	elt2.innerHTML = texte ;

	// remettre le "selected" au bon endroit
	var options = elt2.getElementsByTagName("option") ;
	// Trouver le nblignes-ième élément
	var numero = (nblignes +1) % options.length ;
	//Sélectionner l'élément en question
	var monoption = options[numero] ;
	monoption.setAttribute("selected", true) ;

	eltul.appendChild(elt2) ;

}

function vide_champ(nom) 
{
	// vide le champ d'id proposé
	var elt = document.getElementById(nom) ;
	elt.value = "" ;
}

function ajoute_reperes()
{ // ajoute des repères
	var eltul = document.getElementById("ajoutereperecourbe") ;
	var nblignes = eltul.childElementCount ;
//	alert(nblignes) ;
	var nb_additionnel = 2;
	for(var i=nblignes; i<nblignes+nb_additionnel ; i++) 
	{
		var elt = document.createElement("li") ;
		elt.innerHTML = 'Âge&nbsp;: <input class="data" type="text" name="repere_age_'+i+'" value=""> \
		ou date&nbsp;: <input type="date" name="repere_date_{{ i }}" value=""> \
		Texte associé&nbsp;: <input class="texte" type="text" name="repere_texte_'+i+'" value=""> \
		Tracer&nbsp;: <input type="checkbox" name="repere_trace_'+i+'">\
		Afficher la date&nbsp;: <input type="checkbox" name="repere_affichedate_{{ i }}">' ;
		eltul.appendChild(elt) ;
	}
	
}
