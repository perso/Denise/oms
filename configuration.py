#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### Toutes les "constantes" de config importés pour ce qui concerne les courbes
CONFIG = {}

### La version de l'app
CONFIG["version"] = 2.6
# Nombre de versions anciennes dans le changelog
CONFIG["nb_lignes_changelog"] = 4

# les unités acceptées
CONFIG["liste_unites"] = ["jours", "semaines", "mois", "années"]
CONFIG["liste_typedonnees"] = ["poids","taille"]
CONFIG["unites_typedonnees"] = {"poids": "kg", "taille":"cm"}
# à combien on arrondit par défaut les dnnées
CONFIG["arrondis_typedonnees"] = {"poids": 0.01, "taille": 1}

# Liste des calculs additionnels
CONFIG["extradata"] = ["calculextradata", "calculextratemps"]

# Liste des symboles autorisés pour les enfants (matplotlib).
# Sous forme "item: description"
CONFIG["liste_symboles"] = {"o": "&#x25CF;", "^": "&#x25B2;", "v": "&#x25BC;", "<": "&#x25C0;", ">": "&#x25B6;", "s": "&#x25A0;", "p": "&#11039;", "P": "&#x2795;", "x": "&times;", "D": "&#x25C6;", '$\u2665$':"&#x2665;", '$\u2217$':"&#x2217;" }

CONFIG["nb_fichiers_enfants_add"] = 3 # Nombre d'enfants additionnels au départ

# fichiers

CONFIG["chemin_oms"] = "data_OMS/"
CONFIG["fichiersOMS"] = {}

CONFIG["fichiersOMS"]["poids"] = {
        "perc_garcon": "wfa_boys_p_exp.txt",
        "perc_fille": "wfa_girls_p_exp.txt",
        "z_garcon": "wfa_boys_z_exp.txt",
        "z_fille": "wfa_girls_z_exp.txt",
        "perc_mixte": "wfa_mix_p_exp.txt",
        "z_mixte": "wfa_mix_z_exp.txt",
        }
CONFIG["fichiersOMS"]["taille"] = {
        "perc_garcon": "lhfa_boys_p_exp.txt",
        "perc_fille": "lhfa_girls_p_exp.txt",
        "z_garcon": "lhfa_boys_z_exp.txt",
        "z_fille": "lhfa_girls_z_exp.txt",
        "perc_mixte": "lhfa_mix_p_exp.txt",
        "z_mixte": "lhfa_mix_z_exp.txt",
        }

CONFIG["voyelles"] = ["a", "e", "i", "o", "u", "y"]

# ajouter le chemin
for typed in CONFIG["liste_typedonnees"]:
    
    for (cle,val) in CONFIG["fichiersOMS"][typed].items():
        CONFIG["fichiersOMS"][typed][cle] = CONFIG["chemin_oms"]+val

# Pour les courbes percentiles
CONFIG["liste_data_choisie_p"] = [(5,1),(7,5),(8,10),(10,25),(11,50),(12,75), (14,90),
              (15,95),(17,99)]
CONFIG["liste_data_choisie_p"].sort(reverse = True)

# pour le sigma : (colonne,sigma)
CONFIG["liste_data_choisie_z"] = [(2,-3),(3,-2),(4,-1),(5,0),(6,1),(7,2),(8,3)]
CONFIG["liste_data_choisie_z"].sort(reverse=True)

# config, côté python

# nombre de jours dans les autres unités
CONFIG["jours_dans_mois"] = 30.4375
CONFIG["jours_dans_annee"] = 365.25
CONFIG["jours_dans_semaine"] = 7


## Maxi et mini de "sécurité"
# jours maxi et mini
CONFIG["jours_maxi_courbe"] = round(5.5*CONFIG["jours_dans_annee"])
CONFIG["jours_mini_courbe"] = 10
CONFIG["jours_defaut_donneesvides"]= round(6.4*CONFIG["jours_dans_mois"]) # si données vides, 6 mois et quelque
# poids max (protection)
CONFIG["poids_maxi"] = 80
CONFIG["poids_maxi_conversion"] = 500 #à partir de ce seuil on considère que c'est en grammes
CONFIG["taille_maxi"] = 150
# taille max du nom
CONFIG["longueur_max_nom_bebe"] = 100

#largeurs et hauteurs min et max

CONFIG["largeur_graphique_max"] = 30
CONFIG["largeur_graphique_min"] = 3

CONFIG["hauteur_graphique_max"] = 30
CONFIG["hauteur_graphique_min"] = 2

CONFIG["couleurs"] = {
        "courbe1" : (0,0,1),
        "courbe2" : (0,1,0),
        "courbe3" : (1,0,0),
        "fond": (1,1,1),
        "cadretxt": (0,0,0),
        "grille": (0.5,0.5,0.5)
        }


## Configuration "par défaut" du graphique, tel qu'exporté/importé
DEFAUT = {}

### Taille du graphique par défaut (pouces, en 80 points par pouce)
DEFAUT["largeur"] = 10
DEFAUT["hauteur"] = 7

# Nombre de lignes par défaut dans le formulaire
DEFAUT["nb_data"] = 6

# couleurs par défaut
DEFAUT["couleurs"] = {}
DEFAUT["couleurs"]["courbe1"] = "#0000FF" # bleu
DEFAUT["couleurs"]["courbe2"] = "#00FF00" # vert
DEFAUT["couleurs"]["courbe3"] = "#FF0000" # rouge
DEFAUT["couleurs"]["fond"]= "#FFFFFF" # blanc
DEFAUT["couleurs"]["cadretxt"] = "#000000" # noir
DEFAUT["couleurs"]["courbeenfant"] = "#000000" # noir
DEFAUT["couleurs"]["grille"] = "#7f7f7f" # gris

DEFAUT["symbole"] = "o" # Symbole par défaut


# Remplissage du formulaire, autres
DEFAUT["age_0"]= "0j"
DEFAUT["legende"] = "oui"
DEFAUT["typecourbe"] = "P"
DEFAUT["unite"] = ""
DEFAUT["tracevide"] = ""
DEFAUT["memechelle"] = ""
DEFAUT["positionlegende"] = "hg"
DEFAUT["prolongercourbes"] = ""
DEFAUT["grilleamelio"] = "oui"


DEFAUT["prematurite"] = "0j"
DEFAUT["agecorrige"] = "oui"
#repere = {"typed": "age", "donnee": "", "texte": ""}
DEFAUT["liste_reperes"] = []
DEFAUT["nb_reperes_mini"] = 3 # Nombre minimum de lignes de repères 


# initialiser la config
def config_init():
    c = DEFAUT.copy()
    c["non_sauve"] = {}
    return c
    
    
