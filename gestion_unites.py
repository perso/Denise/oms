#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from configuration import CONFIG
from gestion_erreurs import warning
from math import log

##################### outils pour affichage et choix de l'unité

def choix_unite(maxi):
    """ en fonction de l'âge maxi, on choisit une unité pertinente : jours, semaine, mois, année"""
    if maxi<40:
        return "jours"
    elif maxi <100:
        return "semaines"
    elif maxi<25*CONFIG["jours_dans_mois"]:
        return "mois"
    else:
        return "années"
    
def choix_echelle_data(typedonnees, donneemax):
    """ en fonction du type de données et du maxi (on suppose qu'on part à 0),
    on choisit un intervalle raisonnable
    On renvoie le couple des pas (majeur, mineur)
    (0,0) en cas de problème avec le typedonnnes """

    if typedonnees not in CONFIG["liste_typedonnees"]:
        print("Type de données non valide : "+typedonnees)
        return (0,0)

    if typedonnees == "poids":
        if donneemax>15:
            return (5,1)
        elif donneemax > 10:
            return (1,0.5)
        else:
            return (1,0.2)
    if typedonnees == "taille":
        if donneemax>100:
            return (10,2)
        else:
            return (5,1)
        
def choix_echelle_temps(unite, tempsmaxi):
    """ en fonction de l'unité voulue et du temps maximal (donné dans l'unité),
    on choisit un intervalle raisonnable entre les pas.
    On renvoie le couple des pas (majeur, mineur).
    (0,0) en cas de problème avec l'unité
    On met 0 en 2e arg si pas de grille secondaire"""
    if unite not in CONFIG["liste_unites"]:
        print("Unité non valide ! "+unite)
        return (0,0)
    
    if unite=="jours":
        if tempsmaxi > 60:  # pourquoi mettre en jours ?
            return (30,1)
        elif tempsmaxi >15:
            return (7,1)
        else:
            return (1,0)
    if unite=="semaines":
        if tempsmaxi >50:
            return (5,1)
        elif tempsmaxi > 10:
            return (2,1)
        else:
            return (1,1/7) # On met en jours
    
    if unite=="mois":
        if tempsmaxi > 24:
            return (12,1)
        elif tempsmaxi > 10:
            return (1,0)
        else:
            return (1,0.25) # on met en semaines à peu près (quart de mois)
        
    if unite=="années":
        return (1,1/12) # années / mois
    
def convertitunite(jours,unite,liste_err):
    """ convertit des jours à l'unité voulue
    renvoie des float"""
    if unite=="jours":
        return jours
    elif unite=="mois":
        return jours/CONFIG["jours_dans_mois"]
    elif unite=="années":
        return jours/CONFIG["jours_dans_annee"]
    elif unite=="semaines":
        return jours/CONFIG["jours_dans_semaine"]
    else:
        warning("erreur sur l'unité : "+unite+" On laisse en jours",liste_err)
        return jours

def convertit_tableau(tableau,unite,liste_err):
    """ convertit un tableau de jours en une autre unité.
    Renvoie le nouveau tableau"""
    return [convertitunite(elt,unite,liste_err) for elt in tableau]

def arrondit_donnee(donnee, typed, arrondi=0):
    """ on arrondit la donnée de type typed (à voir dans CONFIG)
    à arrondit près. SI y'a 0 on va voir dans la config.
    Pour l'arrondit on met par ex 1 pour arrondir à 1 pr_s, 0.1 pour arrondir
    au dixième etc"""
    if arrondi==0:
        arrondi = CONFIG["arrondis_typedonnees"][typed]
    if arrondi==0:
        print("Euuuh ça va merder, y'a une div par 0 dans arrondit_donnees !")
    
    d_arr = round(donnee/arrondi)*arrondi
    nbchiffresvoulus = int(log(1/arrondi, 10))+1 # max sur le nombre de chiffres après la virgule qu'on doit avoir
    return round(d_arr, nbchiffresvoulus)