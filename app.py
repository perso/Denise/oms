#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import flask
import io
import matplotlib.pyplot as plt
import base64
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from configuration import config_init,CONFIG
from gestion_erreurs import initialise_erreurs, debug, niveau_debug, warning
import gestion_donnees as donnees
from trace_courbe import cree_figure
import faq as f


def initialise_mode_beta():
    global beta
    hote = flask.request.host
    if hote[:4] == "beta":
        print("** Mode bêta !**")
        return True
    else:
        return False


app = flask.Flask(__name__)

@app.route('/',methods=['POST','GET'])
def index():
    beta=initialise_mode_beta()
    liste_err = initialise_erreurs()    
    val_form = config_init()
    ## Si on a chargé un fichier de données
    if flask.request.method=="POST":
        if 'fichier_donnees' in flask.request.files:
            ## charger les données dans le formulaire
            fichier = flask.request.files['fichier_donnees']
            chaine = fichier.read()
            val_form2 = donnees.fichier_json_vers_configdonnees(chaine,liste_err)
            # Si jamais le formulaire est vide, c'est qu'il y a eu une erreur !
            if val_form2 == {}:
                return flask.render_template("index.html",err=liste_err[2]+liste_err[1]+liste_err[0],valform=val_form, CONFIG=CONFIG, beta=beta)
            val_form.update(val_form2)
            val_form["fichier_importe"] = 1 # Pour noter qu'on a importé un fichier

    return flask.render_template("index.html",err=liste_err[1],valform=val_form, CONFIG=CONFIG, beta=beta)

@app.route('/apropos')
def apropos():
    beta=initialise_mode_beta()
    return flask.render_template("apropos.html",err=[], beta=beta)
 

@app.route("/courbe/<ext>", methods=['POST'])
def courbe_image(ext):
    liste_err = initialise_erreurs()
    data = flask.request.form

    # récupérer les données du formulaire proprement
    config,listes_jours,listes_donnees = donnees.web_vers_python(data,liste_err)
    debug(" * On a récupéré et traité les données du formulaire web, "+str(listes_jours)+str(listes_donnees),liste_err)

    # Gérer les enfants additionnels
    enfants_add = donnees.gere_enfants_additionnels(data, flask.request.files, liste_err)
    debug("Enfants additionnels récupérés : "+str(enfants_add), liste_err)

    # Récupérer le texte à exporter
    texte = donnees.donnees_vers_json(listes_jours["poids"],listes_donnees["poids"],listes_jours["taille"],listes_donnees["taille"], config)
    debug("texte prêt à être exporté : "+texte,liste_err)
    
    # noter le nom de l'enfant pour l'export
    nomenfant = donnees.simplifie_nom(config['nom'])
    # Les noms des autres enfants c'est sympa
    nomsenfantsplus = ""
    for dicoenfant in enfants_add:
        nomsenfantsplus += "_"+donnees.simplifie_nom(dicoenfant["poids"][0]["nom"])

    existe_courbe = False
    # créer les figures
    try:
        debug("création des figures...",liste_err)
        textes_images = {}
        liste_extracalculs = []

        for typed in CONFIG["liste_typedonnees"]:
            if config["tracevide"] == "oui" or listes_jours[typed] != []:
                existe_courbe = True # Une courbe (au moins) a été tracée
                debug("On trace la courbe de "+typed,liste_err)
                fig = cree_figure(config,listes_jours[typed],listes_donnees[typed],typed,liste_extracalculs, liste_err, enfants_add)
                output = io.BytesIO()
                FigureCanvas(fig).print_png(output)
                plt.close(fig)
                textes_images[typed] = base64.b64encode(output.getvalue()).decode("ascii")
            else:
                textes_images[typed] = ""
                
        result = "success"
    except:
        result = "fail"
        liste_err[0].append("Impossible de générer les courbes ! Config : "+str(config))
    
    if not(existe_courbe):
        warning("Aucune courbe n'a été tracée. C'est probablement parce que les données sont vides et que l'option \"tracer les courbes vides\" n'a pas été cochée.", liste_err)
    
    if ext == "b64":
        reponse = { "result":result, 
            "export_txt": texte,
            "nomenfant": nomenfant,
            "nomsenfantsplus": nomsenfantsplus}
        if result == "success":
            reponse["messages"] = liste_err[0]+liste_err[1]
            reponse["calculextra"] = liste_extracalculs
        else:
            reponse["messages"] = liste_err[2]+liste_err[1]+liste_err[0]

        for typed in CONFIG["liste_typedonnees"]:
            reponse["image_"+typed] = textes_images.get(typed,"")        

        return flask.jsonify(reponse)

    elif ext == "png" and result == "success": # à voir cette partie car il faudrait voir ce qu'on y renvoie... vu 
        # qu'il n'y a plus une courbe mais plusieurs. Là ça renverra la dernière générée.
        return flask.Response(output.getvalue(), mimetype='image/png')
    elif ext == "png":
        return flask.render_template("erreur_fatale.html",erreur="Impossible de générer la courbe")
    else:
        return flask.render_template("erreur_fatale.html",erreur="Le type d'image demandé n'est pas reconnu")
    
    
@app.route("/contact")
def contact():
    beta=initialise_mode_beta()
    return flask.render_template("contact.html",err=[], beta=beta)

@app.route("/faq")
def faq():
    beta=initialise_mode_beta()
    
    table_faq = f.lire_fichier_csv_simple(f.fichier_FAQ)
    l_categ, l_categsimple, table_qr = f.extraire_tables_par_cat(table_faq)

    return flask.render_template("faq.html",lcateg=l_categ,lcategsimple= l_categsimple, tableqr=table_qr,err=[], beta=beta)

@app.route("/changelog")
def changelog():
    beta=initialise_mode_beta()
    
    table_version = f.lire_fichier_csv_simple(f.fichier_changelog)
    
    return flask.render_template("changelog.html",table = table_version, nblignes=CONFIG["nb_lignes_changelog"], err=[], beta=beta)

if __name__ == "__main__":
    print("Mode debug maison : "+str(niveau_debug))
    app.run(host='0.0.0.0',debug=True)