#!/usr/bin/env python3
# -*- coding: utf-8 -*-
### Les fonctions et données pour la FAQ, et le changelog

import csv

chemin_data = "data/"

fichier_FAQ = chemin_data+"FAQ_data.txt"
fichier_changelog = chemin_data+"changelog_data.txt"
from gestion_donnees import simplifie_nom


def lire_fichier_csv_simple(fichier):
    """ lire un fichier csv et le renvoyer en table"""
    table=[]
    with open(fichier, "r") as fichier_csv:
        reader_notes = csv.reader(fichier_csv,delimiter=",",dialect="unix")
        for ligne in reader_notes:
            if ligne != []:
                table.append(ligne)
    return table

def extraire_categories(table):
    """ extrait les catégories (1ere colonne du tableau de tableau)
    renvoie sous forme de liste"""
    liste_cat = []
    for ligne in table:
        if not (ligne[0] in liste_cat):
            liste_cat.append(ligne[0])
    return liste_cat

def extraire_tables_par_cat(table):
    """ construit trois tables : une de catégories, une de tables de (q,r), une de catégories simplifées
     categ[i] et categsimple[i] vont correspondre à tableqr[i] en terme de catégorie"""
    categ = extraire_categories(table)
    categsimple = [ simplifie_nom(cat) for cat in categ ]
    tableqr = [ [] for i in range(len(categ)) ]
    
    for ligne in table:
        try:
            cat = ligne[0]
            (q,r) = [ligne[1],ligne[2]]
            ind = categ.index(cat)
            tableqr[ind].append((q,r))
        except:
            print("Impossible de lire la ligne : "+str(ligne))
            
    return (categ, categsimple, tableqr)
