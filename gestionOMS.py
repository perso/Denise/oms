#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 08:59:11 2020

@author: sekhmet
"""
#from gestion_erreurs import erreur, warning, debug
from gestion_couleurs import degrade_choix

import csv



###################################
# Section où on prépare le graphique
# Fonction qui définit le dégradé
def degrade(nb):
    """
    prend en arg un float entre 0 et 100 et renvoie un zoli dégradé (triplet rgb)
    """
    if nb<50:
        # degrade sur le bleu 
        return (0,1*nb/50,1-(nb/50))
    else:
        # dégradé du vert vers le rouge
        return ((nb-50)/50,1-(nb-50)/50,0)
    
# la liste des numéros de colonne, avec leur label et leur couleur
def affichepercentile(pc):
    if pc==50:
        return "50% (Médiane)"
    else:
        return str(pc)+"%"
def afficheecarttype(z):
    if z==0:
        return "Moyenne"
    else:
        #return ("z = "+str(z))
        if z>0:
            chaine = r"$+"+str(z)+" \sigma$"
        else:
            chaine = r"$"+str(z)+"\sigma$"
        return (chaine)

def renvoie_liste_labels(conf,liste_data_choisie_p,liste_data_choisie_z,liste_err):
    """ fabrique les deux listes de labels OMS"""
     #warning(str(conf),liste_err)
    liste_data_labels_p = [(nocol,affichepercentile(pc),degrade_choix(conf["couleurs"]["courbe1"],conf["couleurs"]["courbe2"],conf["couleurs"]["courbe3"],pc)) for (nocol,pc) in liste_data_choisie_p]
    liste_data_labels_z = [(nocol,afficheecarttype(z),degrade_choix(conf["couleurs"]["courbe1"],conf["couleurs"]["courbe2"],conf["couleurs"]["courbe3"],(z+3)/3*50) ) for (nocol,z) in liste_data_choisie_z]
    return liste_data_labels_p,liste_data_labels_z
    
#liste_data_labels= liste_data_labels_z




##################### outils pour lire les données OMS

def convertit_ligne(ligne):
    """ transforme une table en table de nombres"""
    t = []
    for elt in ligne:
        t.append(float(elt))
    return t



def lire_fichier_csv(fichier):
    """ lire un fichier csv et le renvoyer en table"""
    table=[]
    with open(fichier, "r") as fichier_csv:
        reader_notes = csv.reader(fichier_csv,delimiter="\t")
        debut = True
        for ligne in reader_notes:
            if (not debut):
                table.append(convertit_ligne(ligne))
                #table.append(ligne)
            else:
                debut = False
    return table


def extraire_colonne(table,ncol,maxi=-1):
    """ extrait une colonne d'un tableau double, de taille maximum maxi"""
    t = []
    if maxi==-1:
        maxi = len(table)
    for i in range( min(len(table),maxi) ):
        t.append(table[i][ncol])
    return t

